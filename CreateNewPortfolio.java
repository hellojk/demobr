package Revport.org.test;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

public class CreateNewPortfolio {

	public static void main(String[] args) {
		String exePath = "\\\\EIPSDRSF04\\ProfileRedirection$\\KSS\\Desktop\\Workspace\\Chrome73\\chromedriver.exe";
   		System.setProperty("webdriver.chrome.driver", exePath);
   		WebDriver driver = new ChromeDriver();
   		driver.manage().window().maximize();  
   		
   		String url = "https://app.rev004.gtoint.bfsaws.net:50051/rbs/RBSWebMgr?Action=RevportSideNav";
   		driver.get(url);
   		
   		driver.findElement(By.name("User")).sendKeys("admin"); 
        driver.findElement(By.name("Password")).sendKeys("admin");
        driver.findElement(By.xpath("//input[@type='submit']")).click(); 
        
        driver.findElement(By.xpath("//span[@id='Hierarchy_label']")).click();
        driver.findElement(By.xpath("//td[@id='dijit_PopupMenuItem_1_text']")).click();
        driver.findElement(By.xpath("//tr[@id='dijit_MenuItem_5']")).click();

        driver.switchTo().frame("Mainframe");
        driver.findElement(By.xpath(".//input[@name='AddNew'][@value='Create New Portfolio']")).click();
        
        Select businessUnit = new Select(driver.findElement(By.xpath(".//select[@id='BUSINESSUNITID']")));
        businessUnit.selectByVisibleText("ST BU");
        driver.findElement(By.xpath(".//input[@id='ACCOUNTID']")).sendKeys("KSS Automation Port");
        driver.findElement(By.xpath(".//input[@id='EFFECTIVEDATE']")).clear();
        driver.findElement(By.xpath(".//input[@id='EFFECTIVEDATE']")).sendKeys("01/01/2000");
        driver.findElement(By.xpath(".//input[@id='ACCOUNTNAME']")).sendKeys("KSS Automation Portfolio1");
        driver.findElement(By.xpath(".//input[@id='ACCOUNTSHORTNAME']")).sendKeys("KSS Portfolio1");
        
        Select businessWorkGrp = new Select(driver.findElement(By.xpath(".//select[@name='ACCOUNT_BSSCHANNELID']")));
        businessWorkGrp.selectByVisibleText("KS Automation Sun Capital Mgmt_BWG");
        driver.findElement(By.xpath(".//input[@name='INCEPTIONDATE']")).clear();
        driver.findElement(By.xpath(".//input[@name='INCEPTIONDATE']")).sendKeys("01/01/2000");
        driver.findElement(By.xpath(".//input[@name='STARTDATE']")).clear();
        driver.findElement(By.xpath(".//input[@name='STARTDATE']")).sendKeys("01/01/2000");
        driver.findElement(By.xpath(".//input[@id='FIRMNAME']")).sendKeys("KS-Broker ABC1");
        driver.findElement(By.xpath(".//input[@name='BrowseBroker']")).click();
        //driver.findElement(By.xpath(".//td[@class='dojoxGridCell']/a[contains(text(),'KS-Broker ABC1')]")).click();
        driver.manage().timeouts().implicitlyWait(15,TimeUnit.SECONDS);
        if(driver.findElement(By.linkText("KS-Broker ABC1")).isDisplayed())
        {
        	driver.findElement(By.xpath(".//input[@name='Submit'][@value='Apply']")).click();
        }
        else
        {
        	System.out.println("Error in selecting Broker Client Name");
        }
        Select portfolioCurrency = new Select(driver.findElement(By.xpath(".//select[@id='ISOCURRENCYCODE']")));
        portfolioCurrency.selectByVisibleText("USD - US Dollar");
        Select portfolioType = new Select(driver.findElement(By.xpath(".//select[@name='ACCTYPE_BSSLOOKUPTYPEID']")));
        portfolioType.selectByVisibleText("ST External");
        driver.findElement(By.xpath("(.//input[@name='AddRow'])[1]")).click();
        driver.findElement(By.xpath(".//input[@id='LNAME1']")).sendKeys("KS");
        driver.findElement(By.xpath(".//input[@id='FNAME1']")).sendKeys("Automation");
        driver.findElement(By.xpath(".//input[@name='ContactBrowse']")).click();
        driver.manage().timeouts().implicitlyWait(15,TimeUnit.SECONDS);
        if(driver.findElement(By.linkText("KS, Automation")).isDisplayed())
        {
        	driver.findElement(By.xpath(".//input[@name='Submit'][@value='Apply']")).click();
        }
        else
        {
        	System.out.println("Error in selecting contacts");
        }
        Select contactType = new Select(driver.findElement(By.xpath(".//select[@name='CONTACT_BSSLOOKUPTYPEID1']")));
        contactType.selectByVisibleText("KS Owner");
        
        driver.findElement(By.xpath("(.//input[contains(@name,'Save')])[1]")).click();
        
        driver.switchTo().parentFrame();
        driver.findElement(By.xpath("//span[@id='Hierarchy_label']")).click();
        driver.findElement(By.xpath("//td[@id='dijit_PopupMenuItem_1_text']")).click();
        driver.findElement(By.xpath("//tr[@id='dijit_MenuItem_5']")).click();

        driver.switchTo().frame("Mainframe");
        driver.findElement(By.xpath(".//input[@name='QuickSearchString1']")).sendKeys("KSS Automation Port");
        driver.findElement(By.xpath(".//input[@name='QuickSearch']")).click();
        
        driver.manage().timeouts().implicitlyWait(15,TimeUnit.SECONDS);
        Assert.assertEquals(true, driver.findElement(By.linkText("KSS Automation Port")).isDisplayed());
        
        driver.close();
	}

}
