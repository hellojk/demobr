package Revport.org.test;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

public class CreateNewRemitClient {

	public static void main(String[] args) {
		String exePath = "\\\\EIPSDRSF04\\ProfileRedirection$\\KSS\\Desktop\\Workspace\\Chrome73\\chromedriver.exe";
   		System.setProperty("webdriver.chrome.driver", exePath);
   		WebDriver driver = new ChromeDriver();
   		driver.manage().window().maximize();  
   		
   		String url = "https://app.rev004.gtoint.bfsaws.net:50051/rbs/RBSWebMgr?Action=RevportSideNav";
   		driver.get(url);
   		
   		driver.findElement(By.name("User")).sendKeys("admin"); 
        driver.findElement(By.name("Password")).sendKeys("admin");
        driver.findElement(By.xpath("//input[@type='submit']")).click(); 
        
        driver.findElement(By.xpath("//span[@id='Hierarchy_label']")).click();
        driver.findElement(By.xpath("//tr[@id='dijit_MenuItem_2']")).click();
        
        driver.switchTo().frame("Mainframe");
        driver.findElement(By.xpath(".//input[@name='AddNew'][@value='Create New Client']")).click();
        Select businessWorkGrp = new Select(driver.findElement(By.xpath(".//select[@name='BSSCHANNELID']")));
        businessWorkGrp.selectByVisibleText("Global");
        driver.findElement(By.xpath(".//input[@name='FIRMNAME']")).sendKeys("KSS Sun Capital Management Status");
        driver.findElement(By.xpath(".//input[@name='CLIENTFIRMID']")).sendKeys("2158128184");
        Select clientType = new Select(driver.findElement(By.xpath(".//select[@name='TYPE_BSSLOOKUPTYPEID']")));
        clientType.selectByVisibleText("Remit");
        driver.findElement(By.xpath(".//input[@name='INCEPTIONDATE']")).clear();
        driver.findElement(By.xpath(".//input[@name='INCEPTIONDATE']")).sendKeys("01/01/2000");
        driver.findElement(By.xpath(".//td[contains(@class,'tabbg')]/a[contains(text(),'Bank Information')]")).click();
        Select bankName= new Select(driver.findElement(By.xpath(".//select[@fullname='Bank Name']")));
        bankName.selectByVisibleText("ST Cambridge Savings");
        driver.findElement(By.xpath(".//input[@name='BANKACCOUNTNUMBER1']")).sendKeys("9876");
        driver.findElement(By.xpath(".//td[contains(@class,'tabbg')]/a[text()='Contacts']")).click();
        driver.findElement(By.xpath("(.//input[@name='AddRow'])[1]")).click();
        driver.findElement(By.xpath(".//input[@id='LNAME1']")).sendKeys("KS");
        driver.findElement(By.xpath(".//input[@id='FNAME1']")).sendKeys("Automation");
        driver.findElement(By.xpath(".//input[@name='ContactBrowse']")).click();
        driver.manage().timeouts().implicitlyWait(15,TimeUnit.SECONDS);
        if(driver.findElement(By.linkText("KS, Automation")).isDisplayed())
        {
        	driver.findElement(By.xpath(".//input[@name='Submit'][@value='Apply']")).click();
        }
        else
        {
        	System.out.println("Error in selecting contacts");
        }
        Select contactType = new Select(driver.findElement(By.xpath(".//select[@name='CONTACT_BSSLOOKUPTYPEID1']")));
        contactType.selectByVisibleText("KS Owner");
        driver.findElement(By.xpath("(.//input[contains(@name,'Save')])[1]")).click();
        
        driver.switchTo().parentFrame();
        driver.findElement(By.xpath("//span[@id='Hierarchy_label']")).click();
        driver.findElement(By.xpath("//tr[@id='dijit_MenuItem_2']")).click();
        driver.switchTo().frame("Mainframe");
        driver.findElement(By.xpath(".//input[@name='QuickSearchString1']")).sendKeys("KSS Sun Capital Management Status");
        driver.findElement(By.xpath(".//input[@name='QuickSearch']")).click();
        Assert.assertEquals(true, driver.findElement(By.linkText("KSS Sun Capital Management Status")).isDisplayed());
        
        driver.close();
        
	}

}
