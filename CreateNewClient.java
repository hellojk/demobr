package Revport.org.test;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

public class CreateNewClient {

	public static void main(String[] args) {
		String exePath = "\\\\EIPSDRSF04\\ProfileRedirection$\\KSS\\Desktop\\Workspace\\Chrome73\\chromedriver.exe";
   		System.setProperty("webdriver.chrome.driver", exePath);
   		WebDriver driver = new ChromeDriver();
   		driver.manage().window().maximize();  
   		
   		String url = "https://app.rev004.gtoint.bfsaws.net:50051/rbs/RBSWebMgr?Action=RevportSideNav";
   		driver.get(url);
   		
   		driver.findElement(By.name("User")).sendKeys("admin"); 
        driver.findElement(By.name("Password")).sendKeys("admin");
        driver.findElement(By.xpath("//input[@type='submit']")).click(); 
        
        driver.findElement(By.xpath("//span[@id='Hierarchy_label']")).click();
        driver.findElement(By.xpath("//td[@id='dijit_PopupMenuItem_0_text']")).click();
        driver.findElement(By.xpath("//tr[@id='dijit_MenuItem_0']")).click();

        driver.switchTo().frame("Mainframe");
        driver.findElement(By.xpath(".//input[@name='AddNew'][@value='Create New Client']")).click();
        
        Select businessWorkGrp = new Select(driver.findElement(By.xpath(".//select[@name='BSSCHANNELID']")));
        businessWorkGrp.selectByVisibleText("Global");
        
        driver.findElement(By.xpath(".//input[@name='FIRMNAME']")).sendKeys("KSS Automation Broker ABC");
        driver.findElement(By.xpath(".//input[@name='CLIENTFIRMID']")).sendKeys("108108108");
        
        Select clientType = new Select(driver.findElement(By.xpath(".//select[@name='TYPE_BSSLOOKUPTYPEID']")));
        clientType.selectByVisibleText("KS Broker");
        driver.findElement(By.xpath(".//input[@name='INCEPTIONDATE']")).clear();
        driver.findElement(By.xpath(".//input[@name='INCEPTIONDATE']")).sendKeys("01/01/2000");
        
        driver.manage().timeouts().implicitlyWait(15,TimeUnit.SECONDS);
        driver.findElement(By.xpath(".//td[contains(@class,'tabbg')]/a[contains(text(),'Bank Information')]")).click();
        Select bankName= new Select(driver.findElement(By.xpath(".//select[@fullname='Bank Name']")));
        driver.manage().timeouts().implicitlyWait(15,TimeUnit.SECONDS);
        bankName.selectByVisibleText("ST Cambridge Savings");
        driver.findElement(By.xpath("(.//input[contains(@name,'Save')])[1]")).click();
        
        driver.switchTo().parentFrame();
        driver.findElement(By.xpath("//span[@id='Hierarchy_label']")).click();
        driver.findElement(By.xpath("//td[@id='dijit_PopupMenuItem_0_text']")).click();
        driver.findElement(By.xpath("//tr[@id='dijit_MenuItem_0']")).click();
        
        
        driver.switchTo().frame("Mainframe");
        driver.findElement(By.xpath(".//input[@name='QuickSearchString1']")).sendKeys("KSS Automation Broker ABC");
        driver.findElement(By.xpath(".//input[@name='QuickSearch']")).click();
        driver.manage().timeouts().implicitlyWait(15,TimeUnit.SECONDS);
        Assert.assertEquals(true, driver.findElement(By.linkText("KSS Automation Broker ABC")).isDisplayed());
        
        driver.close();
	}

}
