package Revport.org.test;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

public class CreateNewContact {

	public static void main(String[] args) throws InterruptedException {   		
   		//To Run In IE
   		DesiredCapabilities capabilities = new DesiredCapabilities();
   		capabilities.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
   		System.setProperty("webdriver.ie.driver","\\\\EIPSDRSF04\\ProfileRedirection$\\KSS\\Desktop\\Workspace\\IEDriverServer.exe");
   		WebDriver driver = new InternetExplorerDriver(capabilities);
   		driver.manage().window().maximize(); 
   		String url = "https://app.rev004.gtoint.bfsaws.net:50051/rbs/RBSWebMgr?Action=RevportSideNav";
   		driver.get(url);
   		driver.findElement(By.partialLinkText("Continue to this website (not recommended).")).click();
   		
   		//To Run in Chrome
//   		String exePath = "\\\\EIPSDRSF04\\ProfileRedirection$\\KSS\\Desktop\\Workspace\\Chrome73\\chromedriver.exe";
//   		System.setProperty("webdriver.chrome.driver", exePath);
//   		WebDriver driver = new ChromeDriver();
//   		driver.manage().window().maximize(); 
//   		String url = "https://app.rev004.gtoint.bfsaws.net:50051/rbs/RBSWebMgr?Action=RevportSideNav";
//   		driver.get(url);
   		
   		driver.findElement(By.name("User")).sendKeys("admin"); 
        driver.findElement(By.name("Password")).sendKeys("admin");
        driver.findElement(By.xpath("//input[@type='submit']")).click(); 
        
        driver.manage().timeouts().implicitlyWait(15,TimeUnit.SECONDS);
        driver.findElement(By.xpath("//span[@id='Hierarchy_label']")).click();
        driver.findElement(By.xpath("//tr[@id='dijit_MenuItem_7']")).click();
//        driver.switchTo().frame("Mainframe");
        driver.switchTo().frame(driver.findElement(By.xpath(".//iframe[@id='MainFrame']")));
        driver.findElement(By.xpath(".//input[@name='AddNew'][@value='Create New Contact']")).click();
      
        Select businessWorkGrp = new Select(driver.findElement(By.xpath(".//select[@name='BSSCHANNELID']")));
        businessWorkGrp.selectByVisibleText("Global");
        
        driver.findElement(By.xpath(".//input[@name='SALUTATION']")).sendKeys("Mr");
        driver.findElement(By.xpath(".//input[@name='CONTACTNAMEFIRST']")).sendKeys("FirstName2");
        driver.findElement(By.xpath(".//input[@name='CONTACTNAMELAST']")).sendKeys("LastName2");
        
        driver.findElement(By.xpath(".//input[@name='CLIENTCONTACTID']")).sendKeys("0123");
        driver.findElement(By.xpath(".//input[@name='COMPANYNAME']")).sendKeys("CompanyName");
        driver.findElement(By.xpath(".//input[@name='CONTACTEMAIL']")).sendKeys("abc@xyz.com");
        
        Select phoneNum1 = new Select(driver.findElement(By.xpath(".//select[@name='PHONETYPE1']")));
        phoneNum1.selectByVisibleText("KS Home Phone");
        
        driver.findElement(By.xpath(".//input[@name='PHONENUMBER1']")).sendKeys("080123456789");
        driver.findElement(By.xpath(".//input[@id='EFFECTIVEDATE']")).clear();
        driver.findElement(By.xpath(".//input[@id='EFFECTIVEDATE']")).sendKeys("01/01/2000");
        
        Select country = new Select(driver.findElement(By.xpath(".//select[@fullname='Country'][@name='ISOCOUNTRYCODE1']")));
        country.selectByVisibleText("USA - United States");
        
        driver.findElement(By.xpath(".//input[@fullname='Address Line 1'][@name='LINE11']")).sendKeys("320 Congress Street");
        driver.findElement(By.xpath(".//input[@fullname='Address Line 2'][@name='LINE21']")).sendKeys("Floor 7");
        driver.findElement(By.xpath(".//input[@fullname='Active Date'][@name='EFFECTIVEDATE1']")).clear();
        driver.findElement(By.xpath(".//input[@fullname='Active Date'][@name='EFFECTIVEDATE1']")).sendKeys("01/01/2000");
        driver.findElement(By.xpath(".//input[@fullname='City'][@name='CITYNAME1']")).sendKeys("Boston");
        
        Select stateProvince = new Select(driver.findElement(By.xpath(".//select[@fullname='State/Province'][@name='BSSSTATEPROVINCEID1']")));
        stateProvince.selectByVisibleText("MA");
        
        driver.findElement(By.xpath(".//input[@name='POSTALCODE1']")).sendKeys("02110");
        driver.findElement(By.xpath(".//input[@name='POSTALCODEEXT1']")).sendKeys("10");
        
        driver.findElement(By.xpath("(.//input[contains(@name,'Save')])[1]")).click();
        
        driver.switchTo().parentFrame();
        driver.findElement(By.xpath("//span[@id='Hierarchy_label']")).click();
        driver.findElement(By.xpath("//tr[@id='dijit_MenuItem_7']")).click();       
        //driver.switchTo().frame("Mainframe");  
        driver.switchTo().frame(driver.findElement(By.xpath(".//iframe[@id='MainFrame']")));
        driver.findElement(By.xpath(".//input[@fullname='First Name']")).sendKeys("FirstName2");
        driver.findElement(By.xpath(".//input[@fullname='Last Name']")).sendKeys("LastName2");
        driver.findElement(By.xpath(".//input[@name='QuickSearch']")).click();
        driver.manage().timeouts().implicitlyWait(15,TimeUnit.SECONDS);
        String expectedMessage = "LastName2, FirstName2";
        String message = driver.findElement(By.linkText("LastName2, FirstName2")).getText();
        Assert.assertEquals(message, expectedMessage);
        driver.close();
	}

}
