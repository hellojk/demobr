package Revport.org.test;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

public class CreateNewFeeType {

	public static void main(String[] args) {
		String exePath = "\\\\EIPSDRSF04\\ProfileRedirection$\\KSS\\Desktop\\Workspace\\Chrome73\\chromedriver.exe";
   		System.setProperty("webdriver.chrome.driver", exePath);
   		WebDriver driver = new ChromeDriver();
   		driver.manage().window().maximize();  
   		
   		String url = "https://app.rev004.gtoint.bfsaws.net:50051/rbs/RBSWebMgr?Action=RevportSideNav";
   		driver.get(url);
   		
   		driver.findElement(By.name("User")).sendKeys("admin"); 
        driver.findElement(By.name("Password")).sendKeys("admin");
        driver.findElement(By.xpath("//input[@type='submit']")).click(); 
        
        driver.findElement(By.xpath("//span[@id='System_label']")).click();
        driver.findElement(By.xpath("//td[@id='dijit_PopupMenuItem_14_text']")).click();
        driver.findElement(By.xpath("//tr[@id='dijit_MenuItem_89']")).click();
        
        driver.switchTo().frame("Mainframe");
        Select usageMode= new Select(driver.findElement(By.xpath(".//select[@id='USAGEMODE']")));
        usageMode.selectByVisibleText("Revenue");
        driver.findElement(By.xpath(".//input[@name='AddNew'][@value='Create New Fee Type']")).click();
        
        Select businessWorkGrp = new Select(driver.findElement(By.xpath(".//select[@name='BSSCHANNELID']")));
        businessWorkGrp.selectByVisibleText("Global");
        
        driver.findElement(By.xpath(".//input[@name='FEETYPENAME']")).sendKeys("KSS Automation Fee Type");
        driver.findElement(By.xpath(".//input[@name='FEETYPEDESC']")).sendKeys("KSS Automation KSS Base Fee");
        if(driver.findElement(By.xpath(".//input[@name='PROCESSORDER']")).getAttribute("value").equals("100"));
        else
        {
        	driver.findElement(By.xpath(".//input[@name='PROCESSORDER']")).clear();
        	driver.findElement(By.xpath(".//input[@name='PROCESSORDER']")).sendKeys("100");
        }
        for(int i=0;i<6;i++)
        driver.findElement(By.xpath(".//table[@id='RevenueTable']//input[@name='Add Row']")).click();
        driver.findElement(By.xpath(".//table[@id='Revenue2Table']//input[@name='Add Row']")).click();
       
        Select glGroup1 = new Select(driver.findElement(By.xpath(".//select[@name='RevenueGLPRIMARYTYPE1']")));
        glGroup1.selectByVisibleText("Fee");
        Select glGroup2 = new Select(driver.findElement(By.xpath(".//select[@name='RevenueGLPRIMARYTYPE2']")));
        glGroup2.selectByVisibleText("Fee");
        Select glGroup3 = new Select(driver.findElement(By.xpath(".//select[@name='RevenueGLPRIMARYTYPE3']")));
        glGroup3.selectByVisibleText("Accounts Receivable");
        Select glGroup4 = new Select(driver.findElement(By.xpath(".//select[@name='RevenueGLPRIMARYTYPE4']")));
        glGroup4.selectByVisibleText("Accounts Receivable");
        Select glGroup5 = new Select(driver.findElement(By.xpath(".//select[@name='RevenueGLPRIMARYTYPE5']")));
        glGroup5.selectByVisibleText("Accrual");
        Select glGroup6 = new Select(driver.findElement(By.xpath(".//select[@name='RevenueGLPRIMARYTYPE6']")));
        glGroup6.selectByVisibleText("Accrual");
        
        Select chartField = new Select(driver.findElement(By.xpath(".//select[@name='SubRevenueLOOKUPTYPEGROUPID1']")));
        chartField.selectByVisibleText("KSS1 - Chart field");
        
        Select glType1 = new Select(driver.findElement(By.xpath(".//select[@name='RevenueGLTYPEGROUPID1']")));
        glType1.selectByVisibleText("KSS1 - Base Fee");
        Select glType2 = new Select(driver.findElement(By.xpath(".//select[@name='RevenueGLTYPEGROUPID2']")));
        glType2.selectByVisibleText("KSS1 – AR Billed Base Fee");
        Select glType3 = new Select(driver.findElement(By.xpath(".//select[@name='RevenueGLTYPEGROUPID3']")));
        glType3.selectByVisibleText("KSS1 – AR Billed Base Fee");
        Select glType4 = new Select(driver.findElement(By.xpath(".//select[@name='RevenueGLTYPEGROUPID4']")));
        glType4.selectByVisibleText("KSS1 - Cash");
        Select glType5 = new Select(driver.findElement(By.xpath(".//select[@name='RevenueGLTYPEGROUPID5']")));
        glType5.selectByVisibleText("KSS1 - Base Fee");
        Select glType6 = new Select(driver.findElement(By.xpath(".//select[@name='RevenueGLTYPEGROUPID6']")));
        glType6.selectByVisibleText("KSS1 – AR Unbilled Base Fee");
        
        driver.findElement(By.xpath(".//input[@name='RevenueGLFIELDALLOCATION1']")).sendKeys("100");
        driver.findElement(By.xpath(".//input[@name='RevenueGLFIELDALLOCATION2']")).sendKeys("-100");
        driver.findElement(By.xpath(".//input[@name='RevenueGLFIELDALLOCATION3']")).sendKeys("100");
        driver.findElement(By.xpath(".//input[@name='RevenueGLFIELDALLOCATION4']")).sendKeys("-100");
        driver.findElement(By.xpath(".//input[@name='RevenueGLFIELDALLOCATION5']")).sendKeys("-100");
        driver.findElement(By.xpath(".//input[@name='RevenueGLFIELDALLOCATION6']")).sendKeys("100");
        
        driver.findElement(By.xpath("(.//input[contains(@name,'Save')])[1]")).click();
        
        driver.switchTo().parentFrame();
        driver.findElement(By.xpath("//span[@id='System_label']")).click();
        driver.findElement(By.xpath("//td[@id='dijit_PopupMenuItem_14_text']")).click();
        driver.findElement(By.xpath("//tr[@id='dijit_MenuItem_89']")).click();
        
        driver.switchTo().frame("Mainframe");
        driver.findElement(By.xpath(".//input[@name='QuickSearchString1']")).sendKeys("KSS Automation Fee Type");
        driver.findElement(By.xpath(".//input[@name='Search']")).click();
        
        driver.manage().timeouts().implicitlyWait(15,TimeUnit.SECONDS);
        Assert.assertEquals(true, driver.findElement(By.linkText("KSS Automation Fee Type")).isDisplayed());
        
        driver.close();
	}

}
