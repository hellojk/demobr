package Revport.org.test;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

public class CreateNewCalendar {

	public static void main(String[] args) {
		String exePath = "\\\\EIPSDRSF04\\ProfileRedirection$\\KSS\\Desktop\\Workspace\\Chrome73\\chromedriver.exe";
   		System.setProperty("webdriver.chrome.driver", exePath);
   		WebDriver driver = new ChromeDriver();
   		driver.manage().window().maximize();  
   		
   		String url = "https://app.rev004.gtoint.bfsaws.net:50051/rbs/RBSWebMgr?Action=RevportSideNav";
   		driver.get(url);
   		
   		driver.findElement(By.name("User")).sendKeys("admin"); 
        driver.findElement(By.name("Password")).sendKeys("admin");
        driver.findElement(By.xpath("//input[@type='submit']")).click(); 
        
        driver.findElement(By.xpath("//span[@id='System_label']")).click();
        driver.findElement(By.xpath("//td[contains(@id,'dijit_PopupMenuItem_15_text')]")).click();
        driver.findElement(By.xpath("//td[contains(@id,'dijit_MenuItem_97_text')]")).click();

        driver.switchTo().frame("Mainframe");
        driver.findElement(By.xpath(".//input[contains(@name,'AddNew')][contains(@value,'Create New Calendar')]")).click();
        
        driver.findElement(By.xpath(".//input[contains(@name,'CALENDARNAME')]")).sendKeys("KSS Automation Quarterly");
        driver.findElement(By.xpath(".//input[contains(@name,'CALENDARDESC')]")).sendKeys("Automation KS Calendar");
        Select calendartType = new Select(driver.findElement(By.xpath(".//select[contains(@name,'CALTYPE_BSSLOOKUPTYPEID')]")));
        calendartType.selectByVisibleText("Standard");
        
        Select frequency = new Select(driver.findElement(By.xpath(".//select[contains(@name,'BSSFREQUENCYID')]")));
        frequency.selectByVisibleText("Quarterly");
        
        Select opnActProcessing = new Select(driver.findElement(By.xpath(".//select[contains(@name,'OPEN_BSSLOOKUPTYPEID')]")));
        opnActProcessing.selectByVisibleText("Short");
        
        Select clsdActProcessing = new Select(driver.findElement(By.xpath(".//select[contains(@name,'CLOSE_BSSLOOKUPTYPEID')]")));
        clsdActProcessing.selectByVisibleText("Short");
        
        Select holidayCalendar = new Select(driver.findElement(By.xpath(".//select[contains(@name,'BSSHOLIDAYCALENDAR')]")));
        holidayCalendar.selectByVisibleText("No Holiday");
        
        if(driver.findElement(By.xpath(".//input[contains(@name,'MONDAY')]")).isSelected());
        else {
        	driver.findElement(By.xpath(".//input[contains(@name,'MONDAY')]")).click();
        }
        
        if(driver.findElement(By.xpath(".//input[contains(@name,'TUESDAY')]")).isSelected());
        else {
        	driver.findElement(By.xpath(".//input[contains(@name,'TUESDAY')]")).click();
        }
        
        if(driver.findElement(By.xpath(".//input[contains(@name,'WEDNESDAY')]")).isSelected());
        else {
        	driver.findElement(By.xpath(".//input[contains(@name,'WEDNESDAY')]")).click();
        }
        
        if(driver.findElement(By.xpath(".//input[contains(@name,'THURSDAY')]")).isSelected());
        else {
        	driver.findElement(By.xpath(".//input[contains(@name,'THURSDAY')]")).click();
        }
        
        if(driver.findElement(By.xpath(".//input[contains(@name,'FRIDAY')]")).isSelected());
        else {
        	driver.findElement(By.xpath(".//input[contains(@name,'FRIDAY')]")).click();
        }
        
        driver.findElement(By.xpath("(.//input[contains(@name,'Save')])[1]")).click();
        
        driver.switchTo().parentFrame();
        driver.findElement(By.xpath("//span[@id='System_label']")).click();
        driver.findElement(By.xpath("//td[contains(@id,'dijit_PopupMenuItem_15_text')]")).click();
        driver.findElement(By.xpath("//td[contains(@id,'dijit_MenuItem_97_text')]")).click();
        
        driver.switchTo().frame("Mainframe");
        driver.findElement(By.xpath(".//input[contains(@name,'QuickSearchString1')]")).sendKeys("KSS Automation Quarterly");
        driver.findElement(By.xpath(".//input[contains(@type,'button')][contains(@value,'Search')][contains(@class,'formbutton')]")).click();
        Assert.assertEquals("KSS Automation Quarterly", driver.findElement(By.xpath("(.//table[contains(@class,'resulttablebg')]/tbody/tr)[3]/td/a")).getText());
        driver.close();
	}

}
