package Revport.org.test;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;

public class CreateNewHolidayCalendar {

	public static void main(String[] args) {
		String exePath = "\\\\EIPSDRSF04\\ProfileRedirection$\\KSS\\Desktop\\Workspace\\Chrome73\\chromedriver.exe";
   		System.setProperty("webdriver.chrome.driver", exePath);
   		WebDriver driver = new ChromeDriver();
   		driver.manage().window().maximize();  
   		
   		String url = "https://app.rev004.gtoint.bfsaws.net:50051/rbs/RBSWebMgr?Action=RevportSideNav";
   		driver.get(url);
   		
   		driver.findElement(By.name("User")).sendKeys("admin"); 
        driver.findElement(By.name("Password")).sendKeys("admin");
        driver.findElement(By.xpath("//input[@type='submit']")).click(); 
        
        driver.findElement(By.xpath("//span[@id='System_label']")).click();
        driver.findElement(By.xpath("//td[contains(@id,'dijit_PopupMenuItem_15_text')]")).click();
        driver.findElement(By.xpath("//td[contains(@id,'dijit_MenuItem_98_text')]")).click();
        
        driver.switchTo().frame("Mainframe");
        driver.findElement(By.xpath(".//input[contains(@name,'AddNew')][contains(@value,'Create New Holiday Calendar')]")).click();
        driver.findElement(By.xpath(".//input[contains(@name,'CALENDARNAME')]")).sendKeys("KSS Automation Holiday");
        driver.findElement(By.xpath("(.//input[contains(@name,'Save')])[1]")).click();
        
        driver.switchTo().parentFrame();
        
        driver.findElement(By.xpath("//span[@id='System_label']")).click();
        driver.findElement(By.xpath("//td[contains(@id,'dijit_PopupMenuItem_15_text')]")).click();
        driver.findElement(By.xpath("//td[contains(@id,'dijit_MenuItem_98_text')]")).click();
        
        driver.switchTo().frame("Mainframe");
        driver.findElement(By.xpath(".//input[contains(@name,'QuickSearchString1')]")).sendKeys("KSS Automation Holiday");        
        driver.findElement(By.xpath(".//table[contains(@class,'editline')]/tbody/tr/td/table/tbody/tr/td[3]/input[contains(@type,'button')][contains(@value,'Search')][contains(@class,'formbutton')]")).click();
        Assert.assertEquals("KSS Automation Holiday", driver.findElement(By.xpath("(.//table[contains(@class,'resulttablebg')]/tbody/tr)[3]")).getText());
        
        
        driver.close();
	}

}
