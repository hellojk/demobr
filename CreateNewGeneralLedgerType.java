package Revport.org.test;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

public class CreateNewGeneralLedgerType {

	public static void main(String[] args) {
		String exePath = "\\\\EIPSDRSF04\\ProfileRedirection$\\KSS\\Desktop\\Workspace\\Chrome73\\chromedriver.exe";
   		System.setProperty("webdriver.chrome.driver", exePath);
   		WebDriver driver = new ChromeDriver();
   		driver.manage().window().maximize();  
   		
   		String url = "https://app.rev004.gtoint.bfsaws.net:50051/rbs/RBSWebMgr?Action=RevportSideNav";
   		driver.get(url);
   		
   		driver.findElement(By.name("User")).sendKeys("admin"); 
        driver.findElement(By.name("Password")).sendKeys("admin");
        driver.findElement(By.xpath("//input[@type='submit']")).click(); 
        
        driver.findElement(By.xpath("//span[@id='System_label']")).click();
        driver.findElement(By.xpath("//td[@id='dijit_PopupMenuItem_14_text']")).click();
        driver.findElement(By.xpath("//tr[@id='dijit_MenuItem_90']")).click();
        
        driver.switchTo().frame("Mainframe");
        driver.findElement(By.xpath(".//input[@name='AddNew'][@value='Create New General Ledger']")).click();
        
        Select businessWorkGrp1 = new Select(driver.findElement(By.xpath(".//select[@name='BSSCHANNELID']")));
        businessWorkGrp1.selectByVisibleText("Global");
        Select level1 = new Select(driver.findElement(By.xpath(".//select[@name='GLPARENT']")));
        level1.selectByVisibleText("Parent");
        Select ledgerActType1 = new Select(driver.findElement(By.xpath(".//select[@name='GLACCOUNTTYPE_BSSLOOKUPTYPEID']")));
        ledgerActType1.selectByVisibleText("Revenue");
        driver.findElement(By.xpath(".//input[@name='TYPEGROUPNAME']")).sendKeys("KSS5 - Base Fee");
        driver.findElement(By.xpath(".//input[@name='TYPEGROUPDESC']")).sendKeys("Automation Ledger Type");
        driver.findElement(By.xpath(".//input[@name='Add Row']")).click();
        driver.findElement(By.xpath(".//input[@id='TYPENAME1']")).sendKeys("1234");
        driver.findElement(By.xpath(".//input[@id='SORTORDER1']")).sendKeys("1");
        driver.findElement(By.xpath("(.//input[contains(@name,'Save')])[1]")).click();
        
        driver.switchTo().parentFrame();
        driver.findElement(By.xpath("//span[@id='System_label']")).click();
        driver.findElement(By.xpath("//td[@id='dijit_PopupMenuItem_14_text']")).click();
        driver.findElement(By.xpath("//tr[@id='dijit_MenuItem_90']")).click();
        
        driver.switchTo().frame("Mainframe");
        driver.findElement(By.xpath(".//input[@name='AddNew'][@value='Create New General Ledger']")).click();
        
        Select businessWorkGrp2 = new Select(driver.findElement(By.xpath(".//select[@name='BSSCHANNELID']")));
        businessWorkGrp2.selectByVisibleText("Global");
        Select level2 = new Select(driver.findElement(By.xpath(".//select[@name='GLPARENT']")));
        level2.selectByVisibleText("Parent");
        Select ledgerActType2 = new Select(driver.findElement(By.xpath(".//select[@name='GLACCOUNTTYPE_BSSLOOKUPTYPEID']")));
        ledgerActType2.selectByVisibleText("Asset");
        driver.findElement(By.xpath(".//input[@name='TYPEGROUPNAME']")).sendKeys("KSS5 – AR Billed Base Fee");
        driver.findElement(By.xpath(".//input[@name='TYPEGROUPDESC']")).sendKeys("Automation Ledger Type");
        driver.findElement(By.xpath(".//input[@name='Add Row']")).click();
        driver.findElement(By.xpath(".//input[@id='TYPENAME1']")).sendKeys("5678");
        driver.findElement(By.xpath(".//input[@id='SORTORDER1']")).sendKeys("1");
        driver.findElement(By.xpath("(.//input[contains(@name,'Save')])[1]")).click();
        
        driver.switchTo().parentFrame();
        driver.findElement(By.xpath("//span[@id='System_label']")).click();
        driver.findElement(By.xpath("//td[@id='dijit_PopupMenuItem_14_text']")).click();
        driver.findElement(By.xpath("//tr[@id='dijit_MenuItem_90']")).click();
        
        driver.switchTo().frame("Mainframe");
        driver.findElement(By.xpath(".//input[@name='AddNew'][@value='Create New General Ledger']")).click();
        
        Select businessWorkGrp3 = new Select(driver.findElement(By.xpath(".//select[@name='BSSCHANNELID']")));
        businessWorkGrp3.selectByVisibleText("Global");
        Select level3 = new Select(driver.findElement(By.xpath(".//select[@name='GLPARENT']")));
        level3.selectByVisibleText("Parent");
        Select ledgerActType3 = new Select(driver.findElement(By.xpath(".//select[@name='GLACCOUNTTYPE_BSSLOOKUPTYPEID']")));
        ledgerActType3.selectByVisibleText("Asset");
        driver.findElement(By.xpath(".//input[@name='TYPEGROUPNAME']")).sendKeys("KSS5 - Cash");
        driver.findElement(By.xpath(".//input[@name='TYPEGROUPDESC']")).sendKeys("Automation Ledger Type");
        driver.findElement(By.xpath(".//input[@name='Add Row']")).click();
        driver.findElement(By.xpath(".//input[@id='TYPENAME1']")).sendKeys("9123");
        driver.findElement(By.xpath(".//input[@id='SORTORDER1']")).sendKeys("1");
        driver.findElement(By.xpath("(.//input[contains(@name,'Save')])[1]")).click();
        
        
        driver.switchTo().parentFrame();
        driver.findElement(By.xpath("//span[@id='System_label']")).click();
        driver.findElement(By.xpath("//td[@id='dijit_PopupMenuItem_14_text']")).click();
        driver.findElement(By.xpath("//tr[@id='dijit_MenuItem_90']")).click();
        
        driver.switchTo().frame("Mainframe");
        driver.findElement(By.xpath(".//input[@name='AddNew'][@value='Create New General Ledger']")).click();
        
        Select businessWorkGrp4 = new Select(driver.findElement(By.xpath(".//select[@name='BSSCHANNELID']")));
        businessWorkGrp4.selectByVisibleText("Global");
        Select level4 = new Select(driver.findElement(By.xpath(".//select[@name='GLPARENT']")));
        level4.selectByVisibleText("Parent");
        Select ledgerActType4 = new Select(driver.findElement(By.xpath(".//select[@name='GLACCOUNTTYPE_BSSLOOKUPTYPEID']")));
        ledgerActType4.selectByVisibleText("Asset");
        driver.findElement(By.xpath(".//input[@name='TYPEGROUPNAME']")).sendKeys("KSS5 – AR Unbilled Base Fee");
        driver.findElement(By.xpath(".//input[@name='TYPEGROUPDESC']")).sendKeys("Automation Ledger Type");
        driver.findElement(By.xpath(".//input[@name='Add Row']")).click();
        driver.findElement(By.xpath(".//input[@id='TYPENAME1']")).sendKeys("4567");
        driver.findElement(By.xpath(".//input[@id='SORTORDER1']")).sendKeys("1");
        driver.findElement(By.xpath("(.//input[contains(@name,'Save')])[1]")).click();
        
        
        
        driver.switchTo().parentFrame();
        driver.findElement(By.xpath("//span[@id='System_label']")).click();
        driver.findElement(By.xpath("//td[@id='dijit_PopupMenuItem_14_text']")).click();
        driver.findElement(By.xpath("//tr[@id='dijit_MenuItem_90']")).click();
        
        driver.switchTo().frame("Mainframe");
        driver.findElement(By.xpath(".//input[@name='AddNew'][@value='Create New General Ledger']")).click();
        
        Select businessWorkGrp5 = new Select(driver.findElement(By.xpath(".//select[@name='BSSCHANNELID']")));
        businessWorkGrp5.selectByVisibleText("Global");
        Select level5 = new Select(driver.findElement(By.xpath(".//select[@name='GLPARENT']")));
        level5.selectByVisibleText("Child");
        driver.findElement(By.xpath(".//input[@name='TYPEGROUPNAME']")).sendKeys("KSS5 - Chart field");
        driver.findElement(By.xpath(".//input[@name='TYPEGROUPDESC']")).sendKeys("Automation Chart Field");
        driver.findElement(By.xpath(".//input[@name='Add Row']")).click();
        driver.findElement(By.xpath(".//input[@id='TYPENAME1']")).sendKeys("12599");
        driver.findElement(By.xpath(".//input[@id='SORTORDER1']")).sendKeys("1");
        driver.findElement(By.xpath("(.//input[contains(@name,'Save')])[1]")).click();
        
        
        driver.switchTo().parentFrame();
        driver.findElement(By.xpath("//span[@id='System_label']")).click();
        driver.findElement(By.xpath("//td[@id='dijit_PopupMenuItem_14_text']")).click();
        driver.findElement(By.xpath("//tr[@id='dijit_MenuItem_90']")).click();
        
        driver.switchTo().frame("Mainframe");
        driver.findElement(By.xpath(".//input[@name='QuickSearchString1']")).sendKeys("KSS5%");
        driver.findElement(By.xpath(".//input[@name='QuickSearch']")).click();
        
        driver.manage().timeouts().implicitlyWait(15,TimeUnit.SECONDS);
        if(driver.findElement(By.linkText("KSS5 - Base Fee")).isDisplayed() && driver.findElement(By.linkText("KSS5 – AR Unbilled Base Fee")).isDisplayed() && driver.findElement(By.linkText("KSS5 - Cash")).isDisplayed() && driver.findElement(By.linkText("KSS5 – AR Billed Base Fee")).isDisplayed() && driver.findElement(By.linkText("KSS5 - Chart field")).isDisplayed())
        Assert.assertEquals(5, driver.findElements(By.partialLinkText("KSS5")).size());
        else
        	System.out.println("Error - 5 ledgers are not present");
        driver.close();

	}

}
